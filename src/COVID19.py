import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import datetime
from tabulate import tabulate

from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import LSTM


def GetHistogramPorDia(df):
    fechas = df['fecha'].tolist()
    accesos = df['num_casos'].tolist()

    i = len(accesos)

    plt.bar(range(i), accesos, edgecolor='blue')

    plt.xticks(range(i), fechas, rotation=60)
    plt.title("Casos por día")
    plt.ylim(min(accesos) - 1, max(accesos) + 200)

    figure = plt.gcf()  # obtener figura actual
    figure.set_size_inches(15, 10)

    # especificar DPI al grabar
    plt.savefig("img/GraficoCasosDia.png", dpi=100)
    plt.show()


def GetHistogramPorAnno(df, anno):
    # Se filtra por año
    df_anno = df[df['fecha'].dt.year == anno]

    df_aux = df_anno.copy()

    df_aux['fecha'] = df_aux["fecha"].astype("datetime64")

    # La fecha se modifica por el número del mes
    df_aux["fecha"] = df_aux["fecha"].dt.month

    # Se cuenta los accesos por mes
    #mes_count = df_aux.groupby(df_aux["fecha"].dt.month).size().values

    df_sum_num_casos = df_aux.groupby(['fecha']).agg({'num_casos': 'sum'})
    df = df_sum_num_casos.reset_index()

    # Se asocia, a cada mes, su número de accesos
    #df2 = df_aux.drop_duplicates(subset="fecha").assign(Count=mes_count)

    # Se convierten en lista, los meses y el número de accesos
    fechas = df['fecha'].to_list()
    accesos = df['num_casos'].to_list()

    i = len(accesos)

    plt.figure()
    plt.bar(range(i), accesos, edgecolor='blue')

    plt.xticks(range(i), fechas, rotation=0)
    plt.title('Casos por mes del año {}'.format(anno))
    #plt.ylim(min(accesos) - 1, max(accesos) + 5000)
    plt.ylim(0, 500000)

    figure = plt.gcf()  # get current figure
    figure.set_size_inches(8, 6)

    # especificar DPI al grabar
    plt.savefig('img/GraficoCasosMes_{}.png'.format(anno), dpi=100)
    plt.show()

    # df_aux.groupby(df_aux["FechaHora"].dt.month).count().plot(kind="bar")


def GetRNN(units, epochs, training_set, test_set):
    # Las LSTMs son sensibles a la escala de los datos de entrada, especialmente cuando se utilizan las funciones de activación sigmoide (por defecto) o tanh
    # Normalizar el conjunto de entrenamiento
    sc = MinMaxScaler()  # por defecto es 0,1
    training_set = sc.fit_transform(training_set)

    # Seleccionar el conjunto de entrada a la red y el conjunto de salidas,
    # x_train es la entrada, y_train es la salida
    y_nrows = len(training_set)
    x_nrows = y_nrows - 1

    X_train = training_set[0:x_nrows]
    y_train = training_set[1:y_nrows]
    X_train = np.reshape(X_train, (x_nrows, 1, 1))

    # Parte 2 – Construyendo la Red Neural Recurrente
    # Se construye la red neuronal LSTM

    # Inicializando la RNN
    # Se utiliza un modelo continuo, modelo de regresión
    regressor = Sequential()

    # Se añade una capa de entrada LSTM y una capa Dense
    regressor.add(LSTM(units=units, activation='relu', input_shape=(None, 1)))
    regressor.add(Dense(units=8, activation='sigmoid'))

    # Se añade la capa de salida con una única neurona
    regressor.add(Dense(units=1))

    # Se compila la RNN
    # usando el error cuadrático medio
    # MSE para regresión
    regressor.compile(optimizer='rmsprop', loss='mean_squared_error')

    # Se ajusta la RNN al conjunto de entrenamiento
    regressor.fit(X_train, y_train, batch_size=16, epochs=epochs)

    # Una vez entrenado el modelo,  se procede a la predicción y visualización de los resultados:

    nrows = len(test_set)

    # Obtener el número de acessos previsto para los próximos 30 días
    # inputs = test_set
    # Normalizar el conjunto de test
    inputs = test_set
    inputs = sc.transform(inputs)
    inputs = np.reshape(inputs, (nrows, 1, 1))

    predicted_value = regressor.predict(inputs)
    predicted_value = sc.inverse_transform(predicted_value)

    if (nrows > 1):
        # Calcular error RMSE, mean squared error

        testScore = math.sqrt(mean_squared_error(test_set, predicted_value[:, 0]))
        print('Test Score: %.2f RMSE' % (testScore))

        # Visualizando los resultados
        plt.plot(test_set, color='red', label='Casos reales')
        plt.plot(predicted_value, color='blue', label='Casos predichos por la RN')
        plt.title('Predicción de Casos. Epoch={}'.format(epochs))
        plt.xlabel('Tiempo')
        plt.ylabel('Nº Casos')
        plt.xlim(1, 400)
        plt.ylim(0, 40000)
        plt.legend()
        plt.savefig("img/PrediccionCasosRNN{0}.png".format(str(units)), dpi=100)
        #plt.show()
        plt.clf()
        return 0

    else:
        print('Siguiente valor predicho: %.2f' % (predicted_value))
        return predicted_value


def GetError(pred, ultima_num_casos):
    return abs(round(100 - (pred * 100 / ultima_num_casos), 2))


url = 'https://cnecovid.isciii.es/covid19/resources/casos_tecnica_ccaa.csv'

df_csv = pd.read_csv(url)
print(df_csv)
print(df_csv.columns)
print(df_csv.shape)
# (7486, 8), 01/02/2021
# (7885, 8), 21/02/2021

total_casos = df_csv['num_casos'].sum()
print("Total casos desde 21-02-2020: " + str(total_casos))

df = df_csv[['fecha', 'num_casos']]

#df_fecha = df.groupby(['fecha'])
df_sum_num_casos = df.groupby(['fecha']).agg({'num_casos': 'sum'})
df = df_sum_num_casos.reset_index()

#Parse to_datetime
df['fecha'] = pd.to_datetime(df['fecha'])

# Histograma de accesos por día.
# Hay que tener en cuenta que son más de 1700 días, y en un gráfico no con claridad
"""
GetHistogramPorDia(df)
"""

# Para mejor visualización, se hacen 5 gráficos, uno por cada año, por cada mes del año

#GetHistogramPorAnno(df, 2020)
#GetHistogramPorAnno(df, 2021)


# Predicción

# Se selecciona la columna de trabajo: “num_casos”
#training_set_open = training_set.iloc[:,1:2].values
#test_set_open = test_set.iloc[:,1:2].values


# Los dos últimos registros no tienen los datos actualizados
# Se descartan
df = df.iloc[0:-2]

"""
# Conjunto de datos para comparativa de datos reales vs predichos
training_set = df.iloc[:, 1:2].values
test_set = df.iloc[:, 1:2].values

GetRNN(1, 100, training_set, test_set)
GetRNN(2, 150, training_set, test_set)
GetRNN(4, 200, training_set, test_set)
GetRNN(6, 300, training_set, test_set)
GetRNN(8, 200, training_set, test_set)
"""

# Conjunto de datos para predecir siguiente valor
training_set = df.iloc[0:-1, 1:2].values
test_set = df.iloc[len(df)-1:, 1:2].values

ultima_fecha = df.iloc[-1, 0]
ultima_num_casos = df.iloc[-1, 1]
#siguiente_fecha = ultima_fecha + datetime.timedelta(days=1)

next = GetRNN(1, 100, training_set, test_set)
pred = next[0][0]

table = [["RNN 1, 100", pred, GetError(pred, ultima_num_casos)]]

next = GetRNN(2, 150, training_set, test_set)
pred = next[0][0]
table.append(["RNN 2, 150", pred, GetError(pred, ultima_num_casos)])

next = GetRNN(4, 200, training_set, test_set)
pred = next[0][0]
table.append(["RNN 4, 200", pred, GetError(pred, ultima_num_casos)])

next = GetRNN(6, 300, training_set, test_set)
pred = next[0][0]
table.append(["RNN 6, 300", pred, GetError(pred, ultima_num_casos)])

next = GetRNN(8, 200, training_set, test_set)
pred = next[0][0]
table.append(["RNN 8, 200", pred, GetError(pred, ultima_num_casos)])

# Pintar resultados
# Se importa la librería operator para iterar sobre la tabla

import operator
#table = sorted(table, key=operator.itemgetter(1), reverse=True)

with open('img/rnn_results.txt', 'w') as f:

    print("\n")
    print(("################################# {0} #############################").format('Resultados'))
    f.write(("################################# {0} #############################").format('Resultados'))
    f.write("\n")
    print('Última fecha: ' + ultima_fecha.strftime("%Y-%m-%d"))
    print('Num. casos:   ' + str(ultima_num_casos))
    f.write('Última fecha: ' + ultima_fecha.strftime("%Y-%m-%d"))
    f.write("\n")
    f.write('Num. casos:   ' + str(ultima_num_casos))
    f.write("\n")

    print(tabulate(table, headers=['Tipo RNN', "Valor predicho RNN", "Error %"]))
    f.write(tabulate(table, headers=['Tipo RNN', "Valor predicho RNN", "Error %"]))

    print("########################################################################")
    f.write("\n")
    f.write("########################################################################")
    print("\n")