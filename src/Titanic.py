from sklearn import datasets
from sklearn import neighbors
from sklearn import svm
from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import cross_val_score
from sklearn.neural_network import MLPClassifier

from sklearn.metrics import accuracy_score
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from tabulate import tabulate
import numpy as np
import pandas as pd


def get_confusion_matrix_label(tn, fp, fn, tp):
    return "tn: {0} | fp: {1} | fn: {2} | tp: {3}".format(tn, fp, fn, tp)

def write_to_csv(passengerId, resultado):
    df = pd.DataFrame({'PassengerId': passengerId.to_numpy(),
                       'Survived': resultado})
    df.to_csv('data/titanic_submissions.csv', index=False)

####
## function GetClassifiers
def GetClassifiers(passengerId, x_train, y_train, x_test, y_test):
    # Knn
    clf_knn = neighbors.KNeighborsClassifier()
    clf_knn.fit(x_train, y_train)
    resultado=clf_knn.predict(x_test)
    # print(y_test)
    # print(resultado)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    score_knn = clf_knn.score(x_test, y_test)
    # print(("KNeighborsClassifier: {0}").format(score_knn))
    table = [["KNeighbors", round(score_knn, 4), get_confusion_matrix_label(tn, fp, fn, tp), ""]]

    # Support vector machines (SVMs)
    # https://scikit-learn.org/stable/modules/svm.html#classification
    clf_svm = svm.SVC()
    clf_svm.fit(x_train, y_train)
    resultado=clf_svm.predict(x_test)
    # print(y_test)
    # print(resultado)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    score_svm = clf_svm.score(x_test, y_test)
    # print(("SVM: {0}").format(score_svm))
    table.append(["SVM", round(score_svm, 4), get_confusion_matrix_label(tn, fp, fn, tp), ""])

    scores = cross_val_score(clf_svm, x_train, y_train, cv=5)
    table.append(["SVM", round(scores.mean(), 4), "", "with cross_val_score"])


    # Naive Bayes
    # https://scikit-learn.org/stable/modules/naive_bayes.html#gaussian-naive-bayes
    clf_gnb = GaussianNB()
    clf_gnb.fit(x_train, y_train)
    resultado=clf_gnb.predict(x_test)
    # print(y_test)
    # print(resultado)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    score_gnb = clf_gnb.score(x_test, y_test)
    # print(("GaussianNB: {0}").format(score_gnb))
    table.append(["GaussianNB", round(score_gnb, 4), get_confusion_matrix_label(tn, fp, fn, tp),  ""])

    # Decision Trees (DTs)
    # https://scikit-learn.org/stable/modules/tree.html#classification
    """
    clf_dt = tree.DecisionTreeClassifier()
    clf_dt = clf_dt.fit(x_train, y_train)
    score_dt = clf_dt.score(x_test, y_test)
    #print(score_dt)
    tree.plot_tree(clf_dt) 
    """

    clf_dt = DecisionTreeClassifier(max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_dt, x_train, y_train, cv=5)
    # print(("DecisionTreeClassifier: {0}").format(scores))
    score_dt = scores.mean()
    # print(("DecisionTreeClassifier: {0}").format(score_dt))
    table.append(["DecisionTree", round(score_dt, 4), "", "max_depth=None, min_samples_split=2, random_state=0"])

    clf_rf = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_rf, x_train, y_train, cv=5)
    # print(("RandomForestClassifier: {0}").format(scores))
    score_rf = scores.mean()
    # print(("RandomForestClassifier: {0}").format(score_rf))
    table.append(["RandomForest", round(score_rf, 4), "", "n_estimators=10, max_depth=None, min_samples_split=2, random_state=0"])


    clf_rf = RandomForestClassifier(max_depth=2, random_state=0)
    # Entrenamiento del clasificador
    clf_rf.fit(x_train, y_train)
    # Predicción del clasificador para el conjunto de test
    resultado = clf_rf.predict(x_test)

    # Categorización real de los supervivientes (conjunto y_test de entrenamiento)
    #print(y_test)
    # Categorización obtenida por el algoritmo de clasificación
    #print(resultado)
    # Obtención del porcentaje de éxito del algoritmo de clasificación.
    # El valor 0.9497 significa que de los 418 ejemplos que hay en el conjunto x_test,
    # ha sido capaz de predecir correctamente la categoría del 94,97% de esos 418 ejemplos
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    tasaaciertos = clf_rf.score(x_test, y_test)
    table.append(["RandomForest", round(tasaaciertos, 4), get_confusion_matrix_label(tn, fp, fn, tp), "max_depth=2, random_state=0"])

    # With cross_val
    clf_rf = RandomForestClassifier(max_depth=2, random_state=0)
    scores = cross_val_score(clf_rf, x_train, y_train, cv=4)
    score_rf = scores.mean()
    table.append(["RandomForest", round(score_rf, 4), "", "max_depth=2, random_state=0. With cross_val"])


    clf_rf = RandomForestClassifier(max_depth=6, random_state=0)
    clf_rf.fit(x_train, y_train)
    resultado = clf_rf.predict(x_test)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    tasaaciertos = clf_rf.score(x_test, y_test)
    table.append(["RandomForest", round(tasaaciertos, 4), get_confusion_matrix_label(tn, fp, fn, tp), "max_depth=6, random_state=0"])

    clf_et = ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_et, x_train, y_train, cv=5)
    # print(("ExtraTreesClassifier: {0}").format(scores))
    score_et = scores.mean()
    # print(("ExtraTreesClassifier: {0}").format(score_et))
    table.append(["ExtraTrees", round(score_et, 4), "", "_estimators=10, max_depth=None, min_samples_split=2, random_state=0"])

    # Gradient Boosted Decision Trees (GBDT)
    # https://scikit-learn.org/stable/modules/ensemble.html#gradient-tree-boosting
    clf_gbdt = GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0)
    clf_gbdt.fit(x_train, y_train)
    #resultado = clf_gbdt.predict(x_test)
    #write_to_csv(passengerId, resultado)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    score_gbdt = clf_gbdt.score(x_test, y_test)
    # print(("GradientBoostingClassifier: {0}").format(score_gbdt))
    table.append(["GradientBoosting", round(score_gbdt, 4), get_confusion_matrix_label(tn, fp, fn, tp), "n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0"])


    # Neural network models (supervised)
    # https://scikit-learn.org/stable/modules/neural_networks_supervised.html
    clf_rnn = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1)
    clf_rnn.fit(x_train, y_train)
    resultado = clf_rnn.predict(x_test)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    tasaaciertos = clf_rnn.score(x_test, y_test)
    table.append(["Neural network", round(tasaaciertos, 4), get_confusion_matrix_label(tn, fp, fn, tp), "alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1"])

    # Print results
    import operator
    table = sorted(table, key=operator.itemgetter(1), reverse=True)

    print("\n")
    print(("################################# {0} #################################").format('Results'))
    print(tabulate(table, headers=['Classifier', 'Score', "Observations"]))
    print("########################################################################")
    print("\n")

def GetClassifiers2(passengerId, x_train, y_train, x_test):

    # Knn
    clf_knn = neighbors.KNeighborsClassifier()
    scores = cross_val_score(clf_knn, x_train, y_train, cv=5)
    table = [["KNeighbors", round(scores.mean(), 4), ""]]


    # Support vector machines (SVMs)
    # https://scikit-learn.org/stable/modules/svm.html#classification
    clf_svm = svm.SVC()
    scores = cross_val_score(clf_svm, x_train, y_train, cv=5)
    table = [["SVM", round(scores.mean(), 4), ""]]


    # Naive Bayes
    # https://scikit-learn.org/stable/modules/naive_bayes.html#gaussian-naive-bayes
    clf_gnb = GaussianNB()
    scores = cross_val_score(clf_gnb, x_train, y_train, cv=5)
    table = [["GaussianNB", round(scores.mean(), 4), ""]]

    # Decision Trees (DTs)
    # https://scikit-learn.org/stable/modules/tree.html#classification
    clf_dt = tree.DecisionTreeClassifier()
    scores = cross_val_score(clf_dt, x_train, y_train, cv=5)
    table.append(["DecisionTree", round(scores.mean(), 4), ""])


    clf_dt = DecisionTreeClassifier(max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_dt, x_train, y_train, cv=5)
    table.append(["DecisionTree", round(scores.mean(), 4), "max_depth=None, min_samples_split=2, random_state=0"])


    clf_dt = DecisionTreeClassifier(max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_dt, x_train, y_train, cv=5)
    table.append(["DecisionTree", round(scores.mean(), 4), "max_depth=None, min_samples_split=2, random_state=0"])


    clf_rf = RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_rf, x_train, y_train, cv=5)
    table.append(["RandomForest", round(scores.mean(), 4), "n_estimators=10, max_depth=None, min_samples_split=2, random_state=0"])


    clf_rf = RandomForestClassifier(max_depth=2, random_state=0)
    scores = cross_val_score(clf_rf, x_train, y_train, cv=5)
    table.append(["RandomForest", round(scores.mean(), 4), "max_depth=2, random_state=0"])


    clf_rf = RandomForestClassifier(max_depth=6, random_state=0)
    scores = cross_val_score(clf_rf, x_train, y_train, cv=5)
    table.append(["RandomForest", round(scores.mean(), 4), "max_depth=6, random_state=0"])


    clf_rf = RandomForestClassifier(max_depth=16, random_state=2)

    clf_rf.fit(x_train, y_train)
    # Predicción del clasificador para el conjunto de test
    resultado = clf_rf.predict(x_test)
    write_to_csv(passengerId, resultado)

    scores = cross_val_score(clf_rf, x_train, y_train, cv=5)
    table.append(["RandomForest", round(scores.mean(), 4), "max_depth=16, random_state=2"])

    clf_et = ExtraTreesClassifier(max_depth=16, min_samples_split=2, random_state=2)
    scores = cross_val_score(clf_et, x_train, y_train, cv=5)
    table.append(["ExtraTrees", round(scores.mean(), 4), "max_depth=16, min_samples_split=2, random_state=2"])

    clf_et = ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)
    scores = cross_val_score(clf_et, x_train, y_train, cv=5)
    table.append(["ExtraTrees", round(scores.mean(), 4), "n_estimators=10, max_depth=None, min_samples_split=2, random_state=0"])

    # Gradient Boosted Decision Trees (GBDT)
    # https://scikit-learn.org/stable/modules/ensemble.html#gradient-tree-boosting
    clf_gbdt = GradientBoostingClassifier(n_estimators=150, learning_rate=1.0, max_depth=1, random_state=0)
    scores = cross_val_score(clf_gbdt, x_train, y_train, cv=5)
    table.append(["GradientBoosting", round(scores.mean(), 4), "n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0"])

    # Neural network models (supervised)
    # https://scikit-learn.org/stable/modules/neural_networks_supervised.html
    clf_rnn = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1)
    scores = cross_val_score(clf_rnn, x_train, y_train, cv=5)
    table.append(["Neural network", round(scores.mean(), 4), "alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1"])

    # Print results
    import operator
    table = sorted(table, key=operator.itemgetter(1), reverse=True)

    print("\n")
    print(("################################# {0} #################################").format('Results'))
    print(tabulate(table, headers=['Classifier', 'Score', "Observations"]))
    print("########################################################################")
    print("\n")

def GetSVM(passengerId, x_train, y_train, x_test, y_test):

    clf_svm = svm.SVC()
    clf_svm.fit(x_train, y_train)
    scores = cross_val_score(clf_svm, x_train, y_train, cv=5)
    resultado = clf_svm.predict(x_test)
    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    print(get_confusion_matrix_label(tn, fp, fn, tp))

    """
    clf_svm = svm.SVC()

    # KFold Cross Validation approach
    kf = KFold(n_splits=4, shuffle=False)
    kf.split(x_train)

    # Initialize the accuracy of the models to blank list. The accuracy of each model will be appended to this list
    accuracy_model = []

    # Iterate over each train-test split
    for train_index, test_index in kf.split(x_train):
        # Split train-test
        x_train_l, x_test_l = x_train.iloc[train_index], x_train.iloc[test_index]
        y_train_l, y_test_l = y_train.iloc[train_index], y_train.iloc[test_index]
        # Train the model
        model = clf_svm.fit(x_train_l, y_train_l)
        # Append to accuracy_model the accuracy of the model
        accuracy_model.append(accuracy_score(y_test_l, model.predict(x_test_l), normalize=True) * 100)

    # Print the accuracy
    print(accuracy_model)

    #clf_svm.fit(x_train, y_train)
    resultado = clf_svm.predict(x_test)

    tn, fp, fn, tp = confusion_matrix(y_test, resultado).ravel()
    print(get_confusion_matrix_label(tn, fp, fn, tp))
    """
####
"""
Data description:

Survival - Survival (0 = No; 1 = Yes). Not included in test.csv file.              Supervivientes
Pclass - Passenger Class (1 = 1st; 2 = 2nd; 3 = 3rd)                               Clase
Name - Name.                                                                       Nombre
Sex - Sex.                                                                         Sexo
Age - Age.                                                                         Edad
Sibsp - Number of Siblings/Spouses Aboard.                                         Herman@s
Parch - Number of Parents/Children Aboard.                                         Parientes
Ticket - Ticket Number.                                                            Ticket
Fare - Passenger Fare.                                                             Tarifa
Cabin - Cabin.                                                                     Cabina
Embarked - Port of Embarkation (C = Cherbourg; Q = Queenstown; S = Southampton)    Puerta de embarque
"""


# Cargar datos
data_train = pd.read_csv('data/titanic_train.csv')
data_test = pd.read_csv('data/titanic_test.csv')
data_test_survived = pd.read_csv('data/titanic_test_survived.csv')


# Seleccionar columnas

selected_train_columns = ['Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked', 'Ticket']


x_train = data_train[selected_train_columns]
y_train = data_train[['Survived']]
x_test = data_test[selected_train_columns]
y_test = data_test_survived[['Survived']]


# Unir datasets de train

#x_train.shape = (891, 6)  0..890
#x_test.shape = (418, 6)   0..417

frames = [x_train, x_test]
x_full = pd.concat(frames)


#################################################
# NaN
#################################################
# Eliminar NaN => NO RECOMENDABLE
#data_train = data_train.dropna()
#data_test = data_test.dropna()

# Comprobar nulos en cada columna
col_names = x_full.columns.tolist()
for column in col_names:
    print("Valores nulos en <{0}>: {1}".format(column, x_full[column].isnull().sum()))

# Sustituir NaN por la media de la columna
#x_train = x_train.apply(lambda x: x.fillna(x.median()),axis=0)
#x_test = x_test.apply(lambda x: x.fillna(x.median()),axis=0)
#x_train = x_train.fillna(x_train.mean())
#x_test = x_test.fillna(x_test.mean())

x_full = x_full.fillna(x_full.mean())

#################################################
# Variables categóricas => dummies
#################################################

# Cambiar a dummie la variable categórica Sex
dict_Sex = {'male' : 0, 'female' : 1}
x_full['Sex'] = x_full['Sex'].apply(lambda x:dict_Sex[x])

# Cambiar a dummie la variable categórica Embarked
# train

def set_dummies(data_train, data_test, x_train, x_test, column_name):
    print(data_train[column_name].nunique())
    print(data_train[column_name].value_counts())

    dummies_Embarked_train = pd.get_dummies(data_train[[column_name]])

    col_names = dummies_Embarked_train.columns.tolist()
    for column in col_names:
        x_train[column] = dummies_Embarked_train[column]

    # test
    dummies_Embarked_test = pd.get_dummies(data_test[[column_name]])
    col_names = dummies_Embarked_test.columns.tolist()
    for column in col_names:
        x_test[column] = dummies_Embarked_test[column]

    return x_train, x_test

def set_dummies2(x_full, column_name):
    print(x_full[column_name].nunique())
    print(x_full[column_name].value_counts())

    dummies = pd.get_dummies(x_full[[column_name]])

    col_names = dummies.columns.tolist()
    for column in col_names:
        x_full[column] = dummies[column]

    x_full = x_full.drop(columns=[column_name])

    return x_full

x_full = set_dummies2(x_full, 'Embarked')



"""
dict_Plass = {1 : 'A', 2 : 'B', 3: 'C'}
x_full['Pclass'] = x_full['Pclass'].apply(lambda x:dict_Plass[x])

dict_Plass = {'A': 3, 'B' : 2, 'C': 1}
x_full['Pclass'] = x_full['Pclass'].apply(lambda x:dict_Plass[x])
"""

#x_full = set_dummies2(x_full, 'Pclass')

#################################################
# Scale
#################################################
def get_scale(data, column_name):
    column_aux = data[[column_name]]
    min_max_scaler = preprocessing.MinMaxScaler()
    column_scaled = min_max_scaler.fit_transform(column_aux)
    data['{0}_scaled'.format(column_name)] = column_scaled
    return data.drop(columns=[column_name])

"""
x_train = pd.DataFrame(StandardScaler().fit_transform(x_train))
x_test = pd.DataFrame(StandardScaler().fit_transform(x_test))
"""

"""
x_train = get_scale(x_train, 'Fare')
x_train = get_scale(x_train, 'Age')
x_test = get_scale(x_test, 'Fare')
x_test = get_scale(x_test, 'Age')
"""


aux_ticket = x_full['Ticket'].str.split(" ", n = 2, expand = True)

ticket_column = []
for index, row in aux_ticket.iterrows():
    #print(row)
    aux = 0
    if (row[1] == None and row[2] == None):
        aux = row[0]
    elif row[2] == None:
        aux = row[1]
    else:
        aux = row[2]

    try:
        aux = int(aux)
    except:
        aux = np.nan

    ticket_column.append(aux)

x_full['Ticket'] = ticket_column
x_full = x_full.fillna(x_full.mean())


#x_full = pd.DataFrame(StandardScaler().fit_transform(x_full))

x_full = get_scale(x_full, 'Fare')
x_full = get_scale(x_full, 'Age')
x_full = get_scale(x_full, 'Ticket')

# Separar x_train y x_test
x_train = x_full.iloc[0:891, :]
x_test = x_full.iloc[891:, :]

#GetSVM(data_test["PassengerId"], x_train, y_train, x_test, y_test)
#GetClassifiers(data_test["PassengerId"], x_train, y_train, x_test, y_test)
GetClassifiers2(data_test["PassengerId"], x_train, y_train, x_test)


'''
Sin escalado
################################# Results #################################
Classifier          Score  Observations
----------------  -------  --------------------------------------------------------------------
RandomForest       0.9498  max_depth=2, random_state=0
Neural network     0.9354  alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1
GradientBoosting   0.9187  n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0
RandomForest       0.8923  max_depth=6, random_state=0
GaussianNB         0.8684
RandomForest       0.8114  n_estimators=10, max_depth=None, min_samples_split=2, random_state=0
ExtraTrees         0.7935  _estimators=10, max_depth=None, min_samples_split=2, random_state=0
RandomForest       0.7868  max_depth=2, random_state=0. With cross_val
DecisionTree       0.7722  max_depth=None, min_samples_split=2, random_state=0
KNeighbors         0.6722
SVM                0.6555
########################################################################

Con escalado
################################# Results #################################
Classifier          Score  Observations
----------------  -------  --------------------------------------------------------------------
RandomForest       0.9641  max_depth=2, random_state=0
RandomForest       0.9402  max_depth=6, random_state=0
RandomForest       0.8081  n_estimators=10, max_depth=None, min_samples_split=2, random_state=0
ExtraTrees         0.7935  _estimators=10, max_depth=None, min_samples_split=2, random_state=0
RandomForest       0.7856  max_depth=2, random_state=0. With cross_val
DecisionTree       0.7711  max_depth=None, min_samples_split=2, random_state=0
GradientBoosting   0.7057  n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0
SVM                0.6292
KNeighbors         0.5861
Neural network     0.4306  alpha=1e-5, hidden_layer_sizes=(15, 12), random_state=1
GaussianNB         0.3636
########################################################################
'''