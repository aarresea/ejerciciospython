# README #

En el siguiente repositorio se encuentran 4 ejercicios en Python:

## Análisis de sentimientos
	
Realizar un análisis de cómo de positivas o negativas son frases que la gente tuitea sobre 3 restaurantes.

* Conectar con la Api de Twitter
* Buscar los comentarios de 3 cadenas de restaurantes
* Asignar una puntuación positiva, negativa o neutra a cada comentario
* Crear una puntuación media y comparar las puntuaciones de los centros

Archivos:

* *AnalisisSentimientosTwitter.ipynb*
* Carpeta *img*: se guardan algunos gráficos
* Se incluyen los 3 ficheros csv para los análisis de sentimientos:<br />
     *twitter_burgerking.csv, twitter_mcdonalds.csv, twitter_starbucks.csv*
     *twitter_sentiment.csv* (preprocesado)


## Predicción de accesos a un centro
  
Realizar una predicción del número de socios que van a asistir al centro por día. 

* Se proporciona un archivo (.json) con un histórico de accesos de varios años. Para ello se debe de usar Python, la librería *pandas* y cualquier librería externa.
* Dibujar los accesos por día al club, siendo el eje Y el número de accesos y el eje X la fecha. ¿Qué consideraciones se concluyen al extraer los datos de accesos?
* ¿Qué métodos pueden funcionar mejor y porqué?
* Realizar una predicción de los últimos treinta días de histórico. 
* Realizar una predicción sobre los próximo treinta días (que no están en el histórico)

Archivos:

* *AnalisisAccesos.ipynb*
* *requirements.txt* fichero con las dependencias
* Carpeta *img*: se guardan algunos gráficos
* Carpeta *data*:
     No se incluye los datos originales en JSON, porque pesan mucho (>800MB)<br />
     *Accesos_202002271637_infoAccesso.csv*: fichero con la información de accesos al centro (preprocesamiento de datos ya realizado)

Consideraciones:

Una red neuronal se dibuja de la siguiente manera:

![alt text](src/img/RNN.png)

La capa de entrada corresponde con la siguiente línea<br />
*LSTM(units = 4, activation = 'relu', input_shape = (None, 1))*

Y se pueden añadir las capas ocultas que se desee con la siguiente instrucción (OJO, coste computacional muy elevado).<br />
*regressor.add(Dense(units = 8, activation = 'sigmoid'))*

La última, será la capa de salida<br />
*regressor.add(Dense(units = 1))*

El parámetro *units*, significa el número de neuronas que tendrá la capa.
El parámetro *activation*, con qué función se activará la neurona. Una neurona recibe datos de entrada y en función de la función de activación, se activará o no la neurona. Si se activa, pasa a la siguiente capa (o capa de salida).
Para activar una neurona hay diferentes funciones:<br />
*relu* y *sigmoid* son de las más antiguas y populares (para este tipo de problemas, siempre he utilizado estas).
La función sigmoide (se muestra en la gráfica), para una entrada en la neurona da una probabilidad de salida (0,1). Que hará que active o no la neurona.

![alt text](src/img/RNN_activationFunction.png)

*Muchos procesos naturales y curvas de aprendizaje de sistemas complejos muestran una progresión temporal desde unos niveles bajos al inicio, hasta acercarse a un clímax transcurrido un cierto tiempo; la transición se produce en una región caracterizada por una fuerte aceleración intermedia. La función sigmoide permite describir esta evolución.* (Fuente, https://es.wikipedia.org/wiki/Funci%C3%B3n_sigmoide)

Existen otras (documentación de keras, https://keras.io/api/layers/activations/)<br />
**activations**: *relu, sigmoid, softmax, softplus, softsign, tanh, selu, elu, exponential*

El parámetro *input_shape* es una tupla. Donde *None*, indica que se puede esperar cualquier entero positivo (creo) y de dimensión 1.

Se compila la neurona<br />
*regressor.compile(optimizer = 'rmsprop', loss = 'mean_squared_error')*

Para problemas de este tipo, siempre he utilizado esta compilación porque según nos enseñaron, era la que mejor se ajustaba para el cálculo del gradiente.
La red neuronal itera con los datos, calculando la salida y reajustando la función. 
Existen otras que he visto en la documentación de keras (https://keras.io/api/optimizers/):<br />
**optimizers**: *SGD, RMSprop, Adam, Adadelta, Adagrad, Adamax, Nadam,Ftrl*

Entrenar la red:<br />
*regressor.fit(X_train, y_train, batch_size = 16, epochs = epochs)*

*batch_size* es el número de observaciones que se seleccionan para entrenar la red. Seleccionando 16, el algoritmo coge las primeras 16 muestras del conjunto de datos de entrenamiento y entrena la red, luego las siguientes 16 muestras y vuelve a entrenar la red y así hasta el final. Hay que tener cuidado entre el número que se selecciona y la memoria que tenemos para el cómputo.
epochs es el número de iteraciones de entrenamiento de la red.

Como en todo problema computacional, no es cuestión de poner altos valores en los parámetros, porque:

* Puede producirse sobreaprendizaje
* Hay que valorar el coste computacional que implica. Entrenar una red con 20 capas, tiene un coste muy alto, cuando quizás la mejora de resultado que se obtiene, es insignificante.

**NOTA**: el código del ejercicio contiene diferentes partes de código comentados debido al objetivo en sí del propio problema.
Es un problema de data/analytics y se prueban diferentes opciones con el objetivo de conseguir los mejores resultados de predicción.

## Predicción Titanic

Ejercicio de la página Kaggle: https://www.kaggle.com/c/titanic/<br />
El ejercicio consiste en usar técnicas de Machine Learning para crear un modelo que prediga qué pasajeros sobrevivieron al naufragio del Titanic.<br />

Archivos:

* *Titanic.py*

Adjunto imagen con las diferentes submissions:

![alt text](src/img/Kaggle.PNG)

Actualmente existen 13714 submissions y la mejor predicción de mi predicción (0,78708) se encuentra en la posición 1215. 
Cuando se hizo la submission, estaba sobre la posición 300 pero hay que tener en cuenta que eso fue hace un año y posteriormente, habrá habido mejores submissions de otros participantes que hace que haya bajado en el ranking general.

## Predicción Covid-19

El ejercicio consiste en usar técnicas de Machine Learning para crear un modelo que prediga los números de casos de Covid-19.
Desde la siguiente dirección https://cnecovid.isciii.es/covid19/resources/casos_tecnica_ccaa.csv se puede descargar la información relativa a la actual pandemia Covid-19.
Se trata de hacer un ejercicio de preprocesamiento de datos para posteriormente construir el modelo que prediga el número de casos de Covid-19.

Archivos:

* *COVID19.py*

**NOTA**: el código del ejercicio contiene diferentes partes de código comentados debido al objetivo en sí del propio problema.
Es un problema de data/analytics y se prueban diferentes opciones con el objetivo de conseguir los mejores resultados de predicción.
